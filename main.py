import os
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from py_dotenv import read_dotenv
import random
import time
import mpd

def update_status(song, client):
    title = song["title"]
    artist = song["artist"]
    client.users_profile_set(profile={
        "status_text": f"Vibing to {title} ({artist})",
        "status_emoji": ":tardis:"
    })
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
read_dotenv(dotenv_path)

client = WebClient(token=os.environ['TOKEN'])
player = mpd.MPDClient()
player.connect("localhost", "6600")
song = player.currentsong()
while True:
    try:
        player.ping()
    except ConnectionError:
        player.connect("localhost", "6600")
    if "artist" in song and "title" in song:
        update_status(song, client)
    player.idle('playlist')
    song = player.currentsong()
